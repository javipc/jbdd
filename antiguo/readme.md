# JBDD

Esta versión quedará sin continuación, está obsoleta, existe una nueva versión escrita desde cero pero es INCOMPATIBLE. 


## Pequeño manejador de base de datos para PHP
El objetivo es crear una capa que permita a un programa en PHP funcionar
de manera independiente de la base de datos usada.
Por ejemplo:

	$bdusuario->seleccionar('nombre', 'clave');
	
Detrás de la consulta podría estar Mongo DB, o incluso un archivo de texto, 
en éste caso hay una base de datos MariaDB.

	
El programa se compone de tres objetos:

#### BDDTabla 

Representa las consultas.
Almacena información en un arreglo de datos, 
con la información BDDAdaptador construirá consultas, 
para eso ofrece métodos que le permite al adaptador obtener la información que necesita.


#### BDD  
Representa la base de datos.
Es una capa, funciona independiente de la base de datos.
Es la conexión entre BDDTabla, el programador y BDDAdaptador.
Se conecta con BDDAdaptador y le cede la ejecución.


#### BDDAdaptador 
Es quien genera código de base de datos, en caso de cambiar de base de datos es la única clase que se debe cambiar o modificar.
Recibe datos y con ellos crea las consultas y es quien se encarga de ejecutarlas.





### Mini ejemplito:
 

	// agrega jbdd al programa
	require ("jbdd.php");	
	
	// crea el objeto base de datos
	$bd = new BDD("demo");		
	$bd->conectar();
	
	// crea el objeto que manejará los datos
	$usuario = $bd->tabla ('usuarios');
	
	// hacemos la consulta
	$bd->seleccionar($usuario);
	
	// otra forma
	$usuario->seleccionar();
		
	// seleccionar algunas columnas
	$usuario->seleccion ('usuario');
	
	// o mejor varias
	$usuario->seleccion ('clave', 'correo');
	
	// hacemos la consulta
	$bd->seleccionar($usuario);
	
	// otra forma
	$usuario->seleccionar('usuario', 'clave', 'correo');
	
Para más ejemplos ver el archivo demo.php.

	




### Métodos de BDDTabla
<pre>

* constructor ($nombre)

 * borra todo el contenido del objeto BDDTabla
 nuevo () 
 
 * obtiene el nombre o cambia el nombre de la tabla
 nombre () 
 
 
Ingreso de datos
 
 * agrega un dato a una columna
 valor ($columna, $valor) 
 
 * agrega una condición
 condicion ($columna, $comparacion ,$valor) 
 condicion ($columna, $valor) 
 
 * selección de columnas
 columna   (...$columnas) 
 seleccion (...$columnas) 			
 
 * relación con otra tabla
 relacion ($otraTabla, $otraColumna, $miColumna=null) 
 
 * creación de tablas
 tipo_columna ($columna, $tipo = self::BDD_TIPO_caracteres) 
 
 * ingreso de datos por arreglos
 arreglo ($arreglo, ...$indices) 
 
 * limite de resultados
 limite ($limite) 	

 * columna a ordenar
 orden ($columna, $orden) 
 
 * columnas para agrupar
 grupo (...$columnas) 
 
 
Ejecución de consultas, se puede realizar desde el objeto BDD pasado por parámetro una o varias tablas.
 seleccionar (...$columnas) 			
 insertar   () 
 actualizar () 
 eliminar   () 
 crearTabla () 
 tuplas     () 
 total      () 
 afectados  () 
 afectadas  () 
 infoTabla  () 
 agregarColumna ()
 siguienteTupla ()
 crearTablaYColumnas ()
 existe (...$columnas) 
 insertarSiNoExiste (...$columnas) 
 
 </pre>


### Métodos de BDD
 <pre>
 * constructor ($base, $usuario = 'root', $clave = '', $direccion = 'localhost') 
 
 * conectar a la base de datos
 conectar () 
 
 * consulta si esta desconectado
 desconectado () 
 
 * crea la base de datos
 crear () 
 
 * cierra o desconecta la base de datos 
 cerrar ()
 desconectar ()
 
 * libera recursos
 liberar ()
 
 * total de tuplas
 total ()
 
 * total de tuplas afectadas
 afectadas ()
 afectados ()
 
 * tuplas
 tuplas ()
 siguienteTupla ()
 
Ejecución de consultas (recibe como parámetro un objeto BDDTabla), se pueden realizar directamente desde el objeto BDDTabla
 seleccionar ( $tabla ) 
 actualizar  (...$tablas) 
 eliminar    (...$tablas) 
 insertar    (...$tablas) 
 existe      ($tabla, ...$columnas) 
 crearTabla  (...$tablas) 
 infoTabla   ($tabla) 
 agregarColumna      (...$tablas) 
 insertarSiNoExiste  ($tabla, ...$columnas) 
 crearTablaYColumnas (...$tablas) 
 
 * ejecuta una consulta (recibe como parámetro un texto con comandos)
 ejecutar ($consulta) 		

 * muestra los comandos que ejecuta
 visor ($modo = true) 
 
 * muestra una tabla en html
 imprimir () 
</pre>



Por ahora esos son los métodos disponibles.

No es propósito de este programa ejecutar consultas complejas, 
surgió y se creó para uso de mis proyectos y para facilitar la tarea del manejo de base la base de datos.



# Proyectos en los que se utiliza este programa

Seguime 
https://gitlab.com/javipc/seguime

Foto Reloj
https://t.me/relojbot

Listagrama
https://listagrama.000webhostapp.com
https://t.me/listagrama




# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



