<?php


	/*
			DEMOSTRACIÓN 
			Uso del manejador de base de datos JBDD
	*/

	
	
	// se incluye el manejador
	require ('jbdd.php');
	
	
	// se crea el objeto con los datos, nombre de base de datos, opcional usuario, clave y dirección
	$bd = new BDD('demo', 'root', '', 'localhost');
	
	// en este caso, es lo mismo...
	$bd = new BDD('demo');
	
	
	// se activa la visualización de comandos (para ver como funciona todo)	
	$bd->visor();
	
	// crea la base de datos
	$bd->crear();
	
	// conecta a la base de datos
	$bd->conectar();
	
	// verifica si conectó
	if ($bd->desconectado())
		echo 'DESCONECTADO <BR/>';
	
	
	
	
	
	// crear tablas
	
	
	// se crea el objeto que tendrá información de la tabla usuarios	
	$usuario = $bd->tabla ('usuarios');
	
	// define la estructura (para crear tablas)
	$usuario->tipo_columna('id'     , 'id'        );
	$usuario->tipo_columna('usuario', 'caracteres');	
	$usuario->tipo_columna('clave'  , 'caracteres');
	
	
	// se crea el objeto que tendrá información de la tabla comentarios
	$comentario = $bd->tabla ('comentarios');
		
	// define la estructura (para crear tablas)
	$comentario->tipo_columna ('id'        , 'id'        );
	$comentario->tipo_columna ('usuario'   , 'caracteres');
	$comentario->tipo_columna ('comentario', 'texto'     );
	
	
	// crear las tablas
	$bd->crearTabla($comentario);
	$bd->crearTabla($usuario   );
		
	// como los constructores de $usuario y $comentario tienen una base de datos, se puede usar directamente...
	$usuario   ->crearTabla ();
	$comentario->crearTabla ();
		
	// otra forma de hacer lo mismo...
	$bd->crearTabla($usuario, $comentario);
	
	// si se quiere actualizar y crear columnas faltantes
	$usuario->tipo_columna('correo' , 'caracteres');
	$usuario->crearTablaYcolumnas ();
	
	
	


	// Insertar información
	
	// ejemplo de información recibida por $_POST
	$POST = array ('usuario' => 'coco',
				   'clave'   => '1234',
				   'correo'  => 'coco@12.34');
	
		
	
	// para agregar valores 
	$usuario->valor('usuario', $POST['usuario']);
	$usuario->valor('correo' , $POST['correo' ]);
	$usuario->valor('clave'  , $POST['clave'  ]);
	
	// otra forma
	$usuario->valor('usuario', $POST['usuario'])
			->valor('correo' , $POST['correo' ])
			->valor('clave'  , $POST['clave'  ]);
	
	
	// más simple
	// los nombres de la columna en ESTE CASO deben tener el mismo nombre que las claves del arreglo
	$usuario->arreglo ($POST, 'usuario', 'clave', 'correo');
	
	
	// para insertar el usuario	
	$bd->insertarSiNoExiste($usuario, 'usuario');
	
	// como el constructor de $usuario tiene una base de datos, se puede usar directamente...
	$usuario->insertarSiNoExiste('usuario');
	
	
	
	// también se puede agregar valores e insertar directamente 
	$usuario->arreglo ($POST, 'usuario', 'clave', 'correo')
			->insertarSiNoExiste('usuario');
	
	
	// AVISO: ->insertarSiNoExiste(columnas...) mostrará (ejecutará) el comando insertar si luego de la consulta de selección el usuario no existe
	
	
	
	// reutilizar el objeto $usuario
	$usuario->valor('usuario', 'kiko')->valor('clave', '1234')->insertarSiNoExiste();
	$usuario->valor('usuario', 'pipo')->valor('clave', 'hola')->insertarSiNoExiste();
	$usuario->valor('usuario', 'yiyo')->valor('clave', 'hola')->insertarSiNoExiste();
	$usuario->valor('usuario', 'pepe')->valor('clave', 'hola')->insertarSiNoExiste();
	
	
	
	
	
	
	// para consultar las tablas
	
	// hacemos la consulta
	$bd->seleccionar($usuario);
	
	// otra forma
	$usuario->seleccionar();
		
	// mostramos la tabla
	$bd->imprimir ();
	
	
	// seleccionar algunas columnas
	$usuario->seleccion ('usuario');
	
	// o varios parámetros
	$usuario->seleccion ('clave', 'correo');
	
	// hacemos la consulta
	$bd->seleccionar($usuario);
		
	// otra forma, más simple
	$usuario->seleccionar('usuario', 'clave');
	
	
	// con limites
	$usuario->limite (3);	
	
	// ordenando...	
	$usuario->orden ('usuario');	
	
	// agrupación
	$usuario->grupo ('clave');
	
	// realizar la consulta
	$usuario->seleccionar();	
	
	
	
	
	// consultar si existe un usuario con una clave
	// AVISO: ->existe() compara por la tabla de valores, no por la tabla de comparaciones
	// es para usar antes de insertar tuplas
	
	// se crea el objeto que tendrá información de la tabla usuarios
	$usuario = new BDDTabla ('usuarios', $bd);
	
	
	// para agregar valores 
	$existe = $usuario->arreglo ($POST, 'usuario', 'clave')->existe();
	if ($existe)
		echo 'Usuario y clave correcto';
	else
		echo 'Usuario o clave incorrecto';
	 

	// otro ejemplo
	$usuario->valor('usuario', 'coco');
	$usuario->valor('clave'  , 'abcd');
	$existe = $usuario->existe();
	if ($existe)
		echo 'Usuario y clave correcto';
	else
		echo 'Usuario o clave incorrecto';
	



	// unión
	
	// insertar un comentario
	$comentario->valor ('usuario'   , 'pepe');
	$comentario->valor ('comentario', 'hola');
	$comentario->insertar();
	
	// otro comentario de otro usuario
	$comentario->valor ('usuario'   , 'mima');
	$comentario->valor ('comentario', 'chau');
	$comentario->insertar();
	
	// otro comentario de un tercer usuario
	$comentario->valor ('usuario'   , 'coco');
	$comentario->valor ('comentario', 'shhh');
	$comentario->insertar();
	
	
	// un nuevo objeto $usuario
	$usuario = new BDDTabla ('usuarios', $bd);
	
	// relación de la tabla usuario con comentario 
	// sintaxis: $usuario ->relacion ($comentario, 'col-comentario', 'col-usuario');
	$usuario->relacion ($comentario, 'usuario', 'usuario');
	
	// como el nombre de la columna es el mismo en ambas tablas....
	$usuario->relacion ($comentario, 'usuario');
	
	// más parámetros
	$usuario->condicion ('correo', 'coco@12.34');
	
	// ordenado por una columna de una tabla
	$usuario->orden ('clave');
	
	// ordenado por una columna de otra tabla
	$comentario->orden ('usuario');
	
	// proyección de comentario
	$comentario->seleccion ('comentario');
	
	// proyección de usuario 
	$usuario->seleccion('usuario', 'correo');
	
	// ejecutar la consulta
	$bd->seleccionar($usuario);
	
	// otra forma de ejecutar la consulta
	$usuario->seleccionar();
	
	// mostrar la tabla en pantalla
	$bd->imprimir ();
	
	// ¿y las tuplas? aquí, el comando que sea más cómodo
	$tuplas = $bd     ->tuplas();
	$tuplas = $bd     ->seleccionar($usuario);
	$tuplas = $usuario->seleccionar();
	
	// luego de las consultas se obtienen los datos...
	while ($resultado = $bd->siguienteTupla())
		hacerAlgoCon ($resultado['usuario']);
	
	// .... y listo
	$bd->desconectar();


?>
