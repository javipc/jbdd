<?php

    
    
    

    
    
    // Plantillas para crear las consultas genéricas.
	require_once (__DIR__ . '/'    . 'plantilla.php');

    
    

    /*

        30 de enero de 2021

        Representa una base de datos genérica.

        De aquí se extienden bases de datos específicas.

        Contiene métodos comunes.


        existeTupla
        total
        tuplas 


        cambios:
        plantilla -> datos_plano


    */



    class JBDDGenerico {

        // emite salida de comandos y textos en pantalla.
        public $ver = false;

        // arreglo de consultas y textos.
        protected $historial = array ();


        protected $error = false;
        protected $mensaje_error = 'Simplemente no hay conexión.';


        // objeto plantilla
        protected $plantilla = null;

        // En este contexto, "base" es el nombre de la base de datos.
        protected $base = '';

        // objeto conexión a base de datos. (PHP)
        protected $conexion = null;

        protected $nexos = array (
            'nexo_and' => 'AND',
            'nexo_amp' => '&',
            'nexo_pipe' => '|',
            'nexo_coma' => ','
        );

        protected $tipos = array ();


        protected $tuplas = null;
        protected $tupla = null;
        protected $total = -1;

        

        public function __construct () {
            $this->plantilla = new Plantilla ();
        }


        
        protected function generar_error ($mensaje_error = null) {
            $this->error = true;
            $this->mensaje_error = $mensaje_error;
            $this->historial ($mensaje_error, 'error');
        }



        
        public function error () {
            if ($this->conexion == null)
                return true;            
            return $this->error;
        }


        public function mensaje_error () {			
            return $this->mensaje_error;
        }





        protected function existeTupla () {
			if ($this->tuplas == null)
				return false;
			if (is_bool($this->tuplas))
				return false;
			return true;
		}

        
        public function total () {
			return $this->total;
		}
		

        public function tuplas () {
			return $this->tuplas;
        }
        


        public function ejecutar ($consulta) {
			if ($this->conexion == null)
				return null;
			$this->error = false;
			$this->historial  ($consulta);			
								
		}

		


        // Almacena en un arreglo el historial de consultas y errores.
        // se podría agregar la hora.
        public function historial ($texto = null, $tipo = 'consulta') {
            if ($texto === null)			
                return $this->historial;

            $this->historial [] = $texto;

            if ($this->ver)
                switch ($tipo) {
                    case 'consulta':
                        echo "<br/>Consulta: <pre class = 'codigo mensaje'>$texto </pre>" . PHP_EOL;
                    break;
                    case 'error':
                        echo "<br/><span class = 'error'> Error: $texto </span>". PHP_EOL;
                    break;
                    default:
                        echo "<br/><span class = 'mensaje'> $texto </span>". PHP_EOL;
                    break;
                }
        }


        public function ver ($estado = true) {
            $this->ver = $estado;
        }

        





        // permite cambiar comandos (WHERE) por nexos (AND)
            // nexos ($condiciones, "where", "where", "and")
            // recorre el árbol de datos y agrega los posibles nexos que corresponde.

            protected function crear_nexos ($datos, $nuevos_nexos = array()) {
                $nexos = array_merge ($this->nexos, $nuevos_nexos);			
                
                // recorrido para modificar nexos			
                    foreach ($datos as $indice => $dato)
                        foreach ($this->nexos as $clave => $nexo)
                            if (is_array ($datos[$indice]))
                                $datos[$indice] =
                                $this->nexos (
                                    $datos[$indice],
                                    $clave,
                                    $nexo
                                );			
                        
                return $datos;

            }


            // usado por la función que agrega los nexos al arreglo de datos.
            // recorre un arreglo y agrega los nexos que corresponde.
            protected function nexos ($arreglo, $clave_nexo = 'nexo_and', $palabra_nexo = 'AND', $primer_palabra = '') {
                if ($arreglo === null)
                    return array ();
                    
                $respuesta = array();

                foreach ($arreglo as $valor) {
                    $valor [$clave_nexo] = $palabra_nexo;
                    $respuesta [] = $valor;
                }

                if (isset ($respuesta [0] [$clave_nexo]))
                    $respuesta [0] [$clave_nexo] = $primer_palabra;
                    
                return $respuesta;

            }
            




            protected function informacion_extra ($datos, $indice, $valor) {
                $respuesta = array ();                
                foreach ($datos as $dato) {                
                    $dato [$indice] = $valor; 
                    $respuesta [] [$clave] = $valor;
                }
                        
                return $respuesta;
            }



            public function tipo ($tipo, $dimension = null, $extra = null) {

                $info_tipo = array();
                if (isset ($this->tipos[$tipo]))
                    $info_tipo = $this->tipos[$tipo];
                    
                if ($extra == null)
                    if (isset ($this->tipos[$tipo] ['extra']))
                        $extra = $this->tipos[$tipo] ['extra'];

                if ($dimension == null)
                    if (isset ($this->tipos[$tipo] ['dimension']))
                        $dimension = $this->tipos[$tipo] ['dimension'];

                if (isset ($this->tipos[$tipo] ['tipo']))
                    $tipo = $this->tipos[$tipo] ['tipo'];


                
                
                $respuesta = array (
                    'tipo' => $tipo,
                    'dimension' => $dimension,
                    'dimension_parentesis' => '(' . $dimension . ')',
                    'extra' => $extra
                );

                if ($dimension == null)
                    $respuesta ['dimension_parentesis'] = '';

                return $respuesta;
                
            }
    
    
    



            // devuelve una consulta (texto) en lenguaje de base de datos.
            protected function crear_consulta ($tabla, $plantilla_texto, $sufijo) {
                $datos = $tabla->todo ();	
                $consulta = $this->reemplazar ($plantilla_texto, $datos);
                $consulta = $consulta . $sufijo;
                return $consulta;
            }

        
            // realiza el reemplazo de datos sobre una plantilla de texto.
            protected function reemplazar ($plantilla, $datos) {            
                $datos = $this->crear_nexos ($datos);
                $this->plantilla->documento = $plantilla;
                $this->plantilla->datos_plano ($datos);
                $this->plantilla->limpiar();
                return $this->plantilla->documento;
            }
            


            // Remueve código de plantilla no usado (no reemplazado).
            // Esta función no es utilizada
            /*
            protected function limpiar ($plantilla) {
                $this->plantilla->documento = $plantilla;
                $this->plantilla->limpiar();
                return $this->plantilla->documento;
            }
            */




            

            /*
            public function imprimir ($estilo = 'border=1') {
                if ($this->existeTupla() == false) 				
                    return;
                
                $tuplas = $this->tuplas;
                
                $plantilla = 
                '<div class = "{clase}">
                    <table>
                        <tr>
                            {encabezados: <th>{encabezado}</th>}
                        </tr>
                        {filas:
                        <tr>
                            {columnas: <td>{columna}</td>}
                        </tr>
                        }
                    </table>
                </div>
                ';
                            
                
            }

            */


            

            public function html ($lista, $atributo_extra = 'border=1', $encabezado = array()) {


                $html = '<table ' . $atributo_extra . ' >';
                    
                $linea = '';

                foreach ($encabezado as $celda) 
                    $linea = $linea . '<th>' . $celda . '</th>';
                
                if ($encabezado != array())
                    $html = $html . '<tr>' . $linea . '</tr>';

                foreach ($lista as $fila) {
                    $linea = '';
                    foreach ($fila as $celda) {
                        $linea = $linea . '<td>' . $celda . '</td>';
                    }
                    $html = $html . '<tr>' . $linea . '</tr>';

                }


                $html = $html . '</table>';

                return $html;

                
            }



            
        
            


        
    }


















?>
