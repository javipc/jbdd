<?php

	/*
		Es un manejador de tablas (con información de consultas).
		Desde aquí se insertará, actualizará, eliminará y seleccionará datos.
		También se usará para emitir respuestas.

		Esta clase maneja datos genéricos.

		05 / 02 / 2021
		
	*/
	

	
	class JBDDTabla {

		const BDDSELECCION  = 'seleccion';
		const BDDVALOR      = 'valor';
		const BDDCONDICION  = 'condicion';
		const BDDRELACION   = 'relacion';		
		const BDDLIMITE     = 'limite';
		const BDDORDEN      = 'orden';
		const BDDGRUPO      = 'grupo';

		const BDDTIENE      = 'tiene_';
		

		public $nombre = 'usuarios';
		
		
		protected $datos = array ();

		// En este contexto, "base" es un objeto. 
		// Es un manejador de base de datos. Ej: JBDDMySQL
		// Delega consultas a este manejador para realizar consultas directamente en el objeto tabla.		
		private $base = null;


		public function __construct ($nombre, $base) {
			$this->nombre = $nombre;
			$this->datos = ['nombre' => $nombre];
			$this->base = $base;
		}

		public function nombre ($nombre = null) {
			if ($nombre == null)
				return $this->nombre;
			$this->datos ['nombre'] = $nombre;
			$this->nombre = $nombre;
		}

		
		
		public function __toString () {
			return $this->nombre;
		}







		/*
			OPERACIONES 
		*/


		
		// agrega columnas de selección
		// seleccion ("usuario", "clave", "estado", ...);		

		public function seleccion (...$columnas) {
			// si hay un * - se borra, para eviar "select *, usuario" y que quede "select usuario"
			if (isset($this->datos[self::BDDSELECCION][0]['columna']))
				if ($this->datos[self::BDDSELECCION][0]['columna'] == '*')
					$this->datos[self::BDDSELECCION] = array ();

			// si no hay datos de columnas, agrega *
			if ($columnas == array())
				$columnas = array('*');
			

			foreach ($columnas as $columna)				
				$this->datos[self::BDDSELECCION] []  = ['columna' => $columna, 'tabla' =>$this->nombre];
				
			return $this;
		}

		// --------------------------------------------------------
		// ingreso de datos
		// --------------------------------------------------------

		// agrega pares "columna = dato" para insertar o actualizar
		public function valor ($columna, $valor) {
			if ($valor === 'false') $valor = 0;
			if ($valor === 'true')  $valor = 1;

			$nuevo = array (
				'columna'     => $columna,				
				'valor'       => $valor,
				'tabla'		  => $this->nombre
				);

			// evita repetir columnas // permite reutilizar objetos
			$this->quitar (self::BDDVALOR, 'columna', $columna);

			// agrega la columna
			$this->agregar(self::BDDVALOR, $nuevo);
			return $this;
		}



		public function condicion ($columna, $comparacion ,$valor=null) {
			if ($valor === null) {
				$valor = $comparacion;
				$comparacion = '=';
			}

			$nuevo = array (
				'columna'     => $columna,
				'comparacion' => $comparacion,
				'valor'       => $valor,
				'tabla'		  => $this->nombre
				);

			$this->datos[self::BDDCONDICION] [] = $nuevo;
			$this->datos[self::BDDTIENE . self::BDDCONDICION] = true;
			return $this;
		}



		
		// agrega una tabla relacionada y sus columnas en común
		// relacion ($comentarios, "usuario", "id");
		public function relacion ($otraTabla, $otraColumna, $miColumna=null) {
			if ($miColumna == null)
				$miColumna = $otraColumna;
			$nuevo = array (
				'objeto' => $otraTabla,
				'tabla_relacionada' => $otraTabla->nombre,
				'tabla'		  => $this->nombre,
				'columna'=> $miColumna,
				'columna_relacionada'=>$otraColumna);


			$this->combinar ($otraTabla);
				
			$this->datos[self::BDDRELACION] [] = $nuevo;
			$this->datos[self::BDDTIENE . self::BDDRELACION] = true;
			return $this;
		}






		public function limite  ($limite, $inicio = '') {
			$this->datos[self::BDDLIMITE][0] = ['limite' => $limite, 'inicio' => $inicio];
			$this->datos[self::BDDTIENE . self::BDDLIMITE] = true;
			return $this;
		}

		public function orden ($columna, $orden= 'asc') {
			// evita repetir columnas // permite reutilizar objetos
			$this->quitar (self::BDDORDEN, 'columna', $columna);

			$this->agregar(self::BDDORDEN, ['columna' => $columna, 'orden' => $orden, 'tabla' => $this->nombre]);
			
			return $this;
		}

		public function grupo (...$columnas) {
			foreach ($columnas as $columna)
				$this->datos[self::BDDGRUPO] [] = ['columna' => $columna, 'tabla' => $this->nombre];
				$this->datos[self::BDDTIENE . self::BDDGRUPO] = true;
			return $this;
		}



	

		public function columna ($columna = 'comentario', $tipo = 'texto', $dimension = '', $extra = '') {			
			$info_columna = $this->base->tipo ($tipo, $dimension, $extra);
			$info_columna ['tabla'] = $this->nombre	;
			$info_columna ['columna'] = $columna;
			$this->datos[self::BDDSELECCION] []  = $info_columna;
				
			return $this;
		}







		public function columnas      () { return $this->obtener (self::BDDSELECCION ); }
		public function valores       () { return $this->obtener (self::BDDVALOR     ); }
		public function condiciones   () { return $this->obtener (self::BDDCONDICION ); }
		public function relaciones    () { return $this->obtener (self::BDDRELACION  ); }		
		public function limitacion    () { return $this->obtener (self::BDDLIMITE    ); }
		public function agrupacion    () { return $this->obtener (self::BDDGRUPO     ); }
		public function ordenamiento  () { return $this->obtener (self::BDDORDEN     ); }
		public function todo          () { return $this->datos;                      }



		// Código repetitivo - Agrega datos
		private function agregar ($indice, $nuevo_dato) {		

			$this->datos[$indice] [] = $nuevo_dato;
			$this->datos[self::BDDTIENE . $indice] = true;
		}

		// Borra datos
		private function quitar ($tipo, $indice, $valor = null) {
			if (isset ($this->datos [$tipo]) == false)
				return;
			$this->datos [$tipo] = $this-> borrar ($this->datos [$tipo], $indice, $valor);
		}

		// Quita elementos de un arreglo 
		private function borrar ($arreglo, $indice, $valor = null) {
			$respuesta = array ();
			foreach ($arreglo as $fila)
				if (isset ($fila [$indice] ) == false )
					$respuesta [] = $fila;

				else 
					if ($valor != null)
						if ($valor != $fila [$indice])
							$respuesta [] = $fila;
			return $respuesta;
		}


		public function obtener ($indice) {
			if (isset ($this->datos [$indice]))
				return $this->datos [$indice];
			return array ();
		}


		

		// Recorre tablas relacionadas, extrae información y la vuelca en el arreglo (como dato propio)
		// Sirve para que el motor de plantillas encuentre la información para reemplazar.
		public function combinar_relaciones () {
			$relaciones = $this->relaciones ();
			foreach ($relaciones as $relacion) 
				$this->combinar ($relacion ['objeto']);
		}
	

		// Combina información de dos tablas.
		// Los datos combinados sirven para tablas relacionadas
		public function combinar ($tabla) {
			
			$indices = array (
				self::BDDSELECCION,
				self::BDDCONDICION,
				self::BDDGRUPO,
				self::BDDVALOR,
				self::BDDORDEN,
				self::BDDGRUPO,
				self::BDDRELACION );

			foreach ( $indices as $indice) {
				
				$datos = $tabla->obtener ($indice);
				
				if (is_array ($datos)) 
					foreach ($datos as $dato)
						$this->agregar ($indice, $dato);				
						
			}
			
		}




		
		// --------------------------------------------------------
		// comandos
		// --------------------------------------------------------


		// invierte comandos
		// en vez de $base->insertar(tabla);
		// se usa    $tabla->insertar()

		// selección rápida
		public function seleccionar (...$columnas) {
			// si $columnas contiene datos... los procesa
			if ($columnas != array())
				$this->seleccion(...$columnas);
			
			if ($this->columnas () == array())
				$this->seleccion('*');
			$this->base->seleccionar ($this);
			return $this;
		}


		// devuelve objeto
		public function insertar   () { $this->base->insertar   ($this); return $this; }
		public function actualizar () { $this->base->actualizar ($this); return $this; }
		public function eliminar   () { $this->base->eliminar   ($this); return $this; }
		public function crearTabla () { $this->base->crearTabla ($this); return $this; }
		public function infoTabla  () { $this->base->infoTabla  ($this); return $this; }		
		public function agregarColumna      () { $this->base->agregarColumna      ($this); return $this; }
		public function crearTablaYColumnas () { $this->base->crearTablaYColumnas ($this); return $this; }
		public function actualizarOInsertar () { $this->base->actualizarOInsertar ($this); return $this; }
		public function insertarSiNoExiste  (...$columnas) { $this->base->insertarSiNoExiste  ($this, ...$columnas); return $this; }

		


		// Desvía todos los métodos al manejador de base de datos.
		public function __call ($funcion, $argumentos) {
			if ($this->base == null)
				return null;

			return $this->base->$funcion ($this, ...$argumentos);			
		}
		
		/*
		// devuelve valores
		public function total      () { return $this->base->total      ($this); }
		public function afectados  () { return $this->base->afectados  ($this); }
		public function afectadas  () { return $this->base->afectadas  ($this); }
		public function tuplas     () { return $this->base->tuplas     ($this); }
		public function error      () { return $this->base->error      ($this); }

		public function existe (...$columnas)  { return $this->base->existe       ($this, ...$columnas); }
		public function siguienteTupla      () { return $this->base->siguienteTupla      ($this); }
		
		
		public function obtenerLista      (...$columnas) { return $this->base->obtenerLista      (...$columnas);  }
		public function obtenerArreglo    (...$columnas) { return $this->base->obtenerArreglo    (...$columnas);  }

		public function imprimir   ($estilo= "class='tabla'") { $this->base->imprimir  ($estilo); return $this; }
		*/




		// ingresa datos desde un arreglo asociativo
		// $tabla->arreglo ($_POST, 'usuario', 'clave', 'fecha'...);
		public function arreglo ($arreglo, ...$indices) {
		    // si el $indices está vacío, lo llena con claves del arreglo
			if ($indices == array())
				if (is_array($arreglo))
					$indices = array_keys($arreglo);
		
			if (is_array($arreglo))
				foreach ($indices as $indice)
					if (isset ($arreglo[$indice]))
						$this-> valor ( $indice, $arreglo [$indice]);
			return $this;
		}

		public function pagina ($pagina = 1, $filas = 20) {
			if ($pagina <=0)
				$pagina = 1;

			$inicio=$filas*($pagina -1);
			
			return $this-> limite  ($filas, $inicio) ;

		}



		public function nuevo () {
			return new JBDDTabla ($this->nombre, $this->base);
		}



	}
	
	
	
	
	

	

    ?>
