<?php

/*
	

	Plantilla PHP

	por Javier Martínez

	Versión: 30 de Enero de 2021
	

	Últimos cambios:
	texto_plano y datos_plano


	Funciones:

	texto (arreglo_de_textos)
		Reemplaza los datos con doble delimitador.
		Se utiliza para textos del sitio que solo puede variar al traducirlo.
		También para nombres de archivos-
		Se debe invocar primero.

	datos (arreglo_de_datos)
		Reemplaza los datos con delimitador simple.
		Se debe invocar luego de la función texto.

	datos_plano (arreglo_de_datos)
		Lo mismo que la función datos, pero no realiza cambios en el contenido.

	texto_plano (arreglo_de_datos)
		Lo mismo que la función texto, pero no realiza cambios en el contenido.
	
	leer (archivo)
		Carga el archivo de plantilla principal.
	
	delimitador (inicio, fin)
		Personaliza el delimitador de datos por uno elegido.

	delimitadordoble (inicio, fin)
		Personaliza el delimitador de textos por uno elegido.

	limpiar ()
		Quita los delimitadores no reemplazados

	imprimir ()
		Visualiza el documento


	Opciones
	ruta = Ubicación de la plantilla principal.
	archivo = 'nombre de la plantilla.htm'
	documento = '<p>Documento actual en uso</p>'
	html = true // Convierte los textos ingresados en texto HTML
	carpetas = true // los archivos de plantillas se ubican en ruta/de/plantillas/ extensión del archivo.




	Modo de uso:

	HTML
	<p> {{saludo}} </p>


	PHP

	// datos
	$texto ['saludo'] = 'Hola';

	// Crea el objeto con la plantilla
    $plantilla = new Plantilla ('index.htm');
    
    // textos de la página (doble delimitador)
    $plantilla->texto ($texto);     

    // muestra la página
    $plantilla->imprimir ();

*/









	class Plantilla {
		
	
		public  $ruta      = 'plantillas/';
		public  $archivo   = 'archivo.htm';
		public  $documento = '';
		

		public $delimitador_ini = '{';
		public $delimitador_fin = '}';

		public $delimitador_ini_doble = '{{';
		public $delimitador_fin_doble = '}}';

		public $delimitador_iteracion = ':';
		public $delimitador_condicion = '?';

		// convierte los caracteres de los datos ingresados en código HTML
		public $html = true; 

		// separa las plantilas en carpetas
		public $carpetas = true;
	
	
		public function __construct ($archivo = null) {
			if ($archivo == null) 
				return;			
			$this->leer($archivo);				
		}
	
		
		public function leer ($archivo) {			
			
			$this->archivo = $archivo;
			
			$archivo = $this->nombreArchivo ($archivo, $this->ruta);
			
			$this->documento = $this->obtenerArchivo ($archivo);
			$this->incluir ();
		}

		public function documento ($documento = '<p>nada</p>') {
			$this->documento = $documento;
		}
		
		public function agregar ($documento = '<p>nada</p>') {
			$this->documento = $this->documento . $documento;
		}
		
		public function imprimir () {							
			echo $this->documento;
		}


		public function delimitador ($ini, $fin) {
			$this->delimitador_ini = $ini;
			$this->delimitador_fin = $fin;
			$this->delimitadordoble ($ini . $ini, $fin . $fin);
		}
		
		public function delimitadordoble ($ini, $fin) {
			$this->delimitador_ini_doble = $ini;
			$this->delimitador_fin_doble = $fin;
		}



		private function nombreArchivo ($nombre, $ruta = '', $carpeta = '') {
			if (strpos ($nombre, '/'))			
				return $nombre;

			if ($this->carpetas == false)
				$carpeta = '';
									
			if ($carpeta != '')
				$ruta = $ruta . $carpeta . '/';
		
			$nombreArchivo = $ruta . $nombre;			
			
			return $nombreArchivo;
		}
		
		
		private function obtenerArchivo ($nombreArchivo) {									

			if (file_exists($nombreArchivo) == false)
				return '';


			$respuesta = '';
			
			$archivo = fopen($nombreArchivo,'r');
			flock($archivo, LOCK_EX);

			if ($archivo)
				while ($linea = fgets($archivo)) { 
					$respuesta = $respuesta . $linea; 
				}

			fclose($archivo);
			
			return $respuesta;
		}

		
		
		
		
		// Reemplaza constantes en el documento
		// Textos que cambiarán según el idioma
		
		public function texto ($datos = array ('indice' => 'valor') ) {
			$delimitador_ini = $this->delimitador_ini_doble;
			$delimitador_fin = $this->delimitador_fin_doble;
			if ($this->html)
				$datos = $this->html ($datos);
			$this->modificar ($datos, $delimitador_ini, $delimitador_fin);			
		}
		
		
		// Reemplaza variables en el documento
		// Textos que varían según resultados obtenidos (ej: base de datos)
		
		public function datos ($datos = array ('indice' => 'valor')) {
			$delimitador_ini = $this->delimitador_ini;
			$delimitador_fin = $this->delimitador_fin;
			
			if ($this->html)
				$datos = $this->html ($datos);
			$this->modificar ($datos, $delimitador_ini, $delimitador_fin);
			$this->arreglo ($datos);
		}
		
		
		public function datos_plano ($datos = array ('indice' => 'valor')) {
			$delimitador_ini = $this->delimitador_ini;
			$delimitador_fin = $this->delimitador_fin;
						
			$this->modificar ($datos, $delimitador_ini, $delimitador_fin);
			$this->arreglo ($datos);
		}

		public function texto_plano ($datos = array ('indice' => 'valor')) {
			$delimitador_ini = $this->delimitador_ini_doble;
			$delimitador_fin = $this->delimitador_fin_doble;
						
			$this->modificar ($datos, $delimitador_ini, $delimitador_fin);
			$this->arreglo ($datos);
		}

		// compatibilidad con versiones anteriores
		public function plano ($datos = array ('indice' => 'valor')) {
			$this->texto_plano();
		}
		

		
		// modifica el documento
		public function modificar ($datos = array ('indice' => 'valor'), $delimitador_ini = '{', $delimitador_fin = '}') {
			$texto = $this->documento;
			$this->documento = $this->reemplazar ($texto, $datos, $delimitador_ini, $delimitador_fin);
		}
		


		// algoritmo de reemplazo 
		// recorre los datos introducidos y reemplaza en el documento

		private function reemplazar ($texto = 'hola {usuario}', $datos = array ('usuario' => 'pepe'), $delimitador_ini = '{', $delimitador_fin = '}') {

			$nuevoTexto = $texto;
			foreach ($datos as $indice => $valor) 
				if (is_array ($valor) == false)
					$nuevoTexto = str_ireplace ($delimitador_ini . $indice . $delimitador_fin, $valor, $nuevoTexto);			
					
		
			return $nuevoTexto;
		}




		
		
		


		// quita los datos vacíos del documento, etiquetas no reemplazadas.
		public function limpiar () {

			// quita delimitadores dobles
			$delimitador_ini = $this->delimitador_ini_doble;
			$delimitador_fin = $this->delimitador_fin_doble;
			// $this->documento = str_ireplace ($delimitador_ini, '', $this->documento);
			// $this->documento = str_ireplace ($delimitador_fin, '', $this->documento);			
			$this->documento = preg_replace ('~' . $delimitador_ini . '(.*?)' . $delimitador_fin . '~', '$1', $this->documento);


			// quita delimitadores simples y su contenido
			
			//$this->documento = preg_replace ('~' . $delimitador_ini . '(.*?)' . $delimitador_fin . '~', '', $this->documento);

					$delimiter_wrap  = '~';
					$iz = $this->delimitador_ini;
					$de = $this->delimitador_fin;
					
					$patron = $delimiter_wrap 
					. $iz
					. '(([^' 
					. $iz 
					. $de 
					. ']++|(?R))*)'
					. $de 
					. $delimiter_wrap;


					$this->documento = preg_replace ($patron, '', $this->documento);

		}

	
				

		


		
		
		
		public function incluir () {
			$iz = $this->delimitador_ini_doble;
			$de = $this->delimitador_fin_doble;
			$extensiones[] = 'htm';
			$extensiones[] = 'txt';
			$extensiones[] = 'svg';	
			$extensiones[] = 'xml';
			$extensiones[] = 'css';
			$extensiones[] = 'js';			
			$extensiones[] = 'json';
			$extensiones[] = 'html';
			
			
			$cambios = false;
			

			foreach ($extensiones as $extension) {
				$expresion = '~'. $iz . '[a-z]+.' . $extension . '+' . $de . '~';
				if (preg_match_all($expresion, $this->documento, $etiquetas)) 
					foreach ($etiquetas[0] as $etiqueta) {
						$nombre_archivo = str_replace ($iz, '', $etiqueta);
						$nombre_archivo = str_replace ($de, '', $nombre_archivo);
						$nombre_archivo = $this->nombreArchivo ($nombre_archivo, $this->ruta, $extension);
						$doc = $this->obtenerArchivo ($nombre_archivo);
						$this->documento = str_ireplace ($etiqueta, $doc, $this->documento);
						
						$cambios = true;
					}
			}
			
			
			if ($cambios)
				$this->incluir();
			
			
		}



















		// iteraciones




		// recupera las iteraciones en una cadena => 
		// {iteracion1 : {iteracion2: ... {} }}

		private function recorrer ($cadena) {
			

					$delimiter_wrap  = '~';
					$iz = $this->delimitador_ini;
					$de = $this->delimitador_fin;
					
					$patron = $delimiter_wrap 
					. $iz
					. '(([^' 
					. $iz 
					. $de 
					. ']++|(?R))*)'
					. $de 
					. $delimiter_wrap;


						
					preg_match_all( $patron, $cadena, $coincidencias);


					
					if (isset ($coincidencias [1]) == false)
						return array();


					$delimitadores[] = $this->delimitador_condicion;
					$delimitadores[] = $this->delimitador_iteracion;
					$respuesta = array ();
					foreach ($delimitadores as $delimitador)
					foreach ($coincidencias[1] as $coincidencia) {
						$posicion_delimitador = strpos($coincidencia, $delimitador);
						if ($posicion_delimitador) {
							$clave = substr($coincidencia, 0, $posicion_delimitador);
							if (strpos ($clave, ' '))
								continue;
							$valor = substr($coincidencia, $posicion_delimitador +1);
							$respuesta[] = [
								'clave' => $clave,
								'valor' => $valor,
								'delimitador' => $delimitador
							];
						}
					}


					return $respuesta;

			}





			private function bucle ($documento, $datos = array ())  {
				$iteraciones = $this->recorrer ($documento);
								
				foreach ($iteraciones as $iteracion) 
					if (isset ($datos[ $iteracion['clave'] ]))
						{
							$clave = $iteracion ['clave'];
							$valor = $iteracion ['valor'];
							$delimitador = $iteracion ['delimitador'];

							$texto = '';
							$texto_original  = $valor;

							if ($delimitador == $this->delimitador_condicion)
							
								if ($datos [$clave]) {
									$texto = $texto . $this->reemplazar ($valor, $datos);
									$texto = $this->bucle ($texto, $datos);
								}

							// reemplaza sus valores internos
							if (is_array ($datos[$clave] ) )
							foreach ($datos[$clave] as $dato) {
								$texto = $texto
									 
									 . $this->reemplazar ($valor, $dato);

								// reemplaza el bloque de iteración
								// recursión
								
									$texto = $this->bucle ($texto, $dato);
																									
								
							}
								
							
							
							if ($texto != '') {
							
								$documento = str_ireplace (
									'{' . $clave 
										. $delimitador . ' '
										. $valor 
										. '}'
									, $texto, $documento);


								$documento = str_ireplace (
									'{' . $clave 
										. $delimitador
										. $valor 
										. '}'
									, $texto, $documento);

							}
							
						}


				// si no hay cambios devuelve el documento original								

				return $documento;
			}



			public function arreglo ($datos = array()) {
				if ($datos == array())
					return;
			

				$this->documento = $this->bucle ($this->documento, $datos);				
	
			}







		// ------------------------------


		public function html ($arreglo) {

			return array_map (
				function ($x) {
					if (is_array ($x))
						return $this->html ($x);
					return htmlentities ($x);
				}, $arreglo);		

			}

	}

?>