<?php



    /*
		FUNCIONES EXTRAS PARA MYSQL
		
		insertar_si_no_existe
		actualizar_o_insertar
		existe
		obtener_arreglo
		obtener_lista
		imprimir


    */

    require_once (__DIR__ . '/'    . 'jbddmysql.php');


    class JBDDMySQLExtra extends JBDDMySQL {

        
        // compara si existen los datos antes de insertar        
        // si no se especifica columnas a comparar, compara con todas las columnas
        // la comparación se hace convirtiendo "valores" en "condiciones" y verifica si ya existen.
		public function insertar_si_no_existe ($tabla, ...$columnas) {
			$this->bloquear();
			if ($this->existe($tabla, ...$columnas) == false)
				$this->insertar($tabla);
			return $this->desbloquear();
		}


		// compara si existen los datos antes de actualizar
		// si existe actualiza, si no existe inserta
		// la comparación se hace usando las propias condiciones de actualización.
		public function actualizar_o_insertar ($tabla) {
            $this->bloquear();
                        
            $this->seleccionar($tabla);
            
            // si existe la fila, actualizará
            // en caso de que no exista, la creará			
			if ($this->total() > 0) 
                $this->actualizar($tabla);
            else                
			    $this->insertar($tabla);
				
			return $this->desbloquear();
		}













		// verifica si una tabla con valores existe
		// compara por valores, no por condiciones
		// si no se especifica columnas a comparar, compara con todas las columnas
		public function existe ($tabla, ...$columnas) {
			// crea una tabla temporal para hacer la consulta sobre columnas especificas
            $tablaTemporal = $tabla->nuevo();
            
            $valores = $tabla->valores ();
			
			// si columnas es un arreglo vacío, pone todas las columnas.
            if ($columnas == array()) 
                foreach ($valores as $valor)
                    $columnas [] = $valor['columna'] ;
			
			
			// recorre las columnas (valores)
			foreach ($valores as $valor)
				foreach ($columnas as $columna)
					if ($valor['columna'] == $columna) 
						$tablaTemporal->condicion ($columna, $valor ['valor']);
					
				
					
				                    

            $tablaTemporal->seleccionar();
            
			return $tablaTemporal->total() > 0;
		}












		public function obtener_arreglo ($tabla, ...$columnas) {
			if ($this->tuplas == null)
				return array();
			
			$arreglo = array();
			
			if ($columnas == array())
				$columnas = array_keys ($this->tupla);
			
			foreach ($columnas as $columna) 
				$arreglo[$columna] = $this->tupla [$columna];

			return $arreglo;
		}
		
		public function obtener_lista ($tabla,...$columnas) {
			if ($this->tuplas == null)
				return array();
			
			$lista = array ();
			
			while ($this->siguienteTupla()) 
				$lista [] = $this->obtener_arreglo ($tabla,...$columnas);				
			
			return $lista;
		}




		public function columna_encabezado ($columna, $titulo) {

		}


		public function imprimir ($tabla, $atributo_extra = 'border=1') {
			$lista = $this->obtener_lista ($tabla);
			$encabezado = array();
			if (isset ($lista[0]))
				$encabezado = array_keys ($lista[0]);
			$html = $this->html ($lista, $atributo_extra, $encabezado);
			echo $html;
		}

    }

?>