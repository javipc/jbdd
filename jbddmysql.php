<?php

	
	/*
        Adaptador MySQL
        Clase manejadora de MySQL

		Convierte datos genéricos (tablas) en consultas MySQL
		
		total (afectados)
		liberar
		desconectar
		siguienteTupla
		bloquear
		desbloquear


	*/
	
	require_once (__DIR__ . '/'    . 'jbddgenerico.php');
    

	class JBDDMySql extends JBDDGenerico {
		
		const IGUAL       = '=';
		const DISTINTO    = '!=';
		const MAYOR       = '>';
		const MENOR       = '<';
		const MAYOR_IGUAL = '>=';
		const MENOR_IGUAL = '<=';
		const CONTIENE    = 'like';
		const ENTRE       = 'between';
				
		const Y           = 'AND';
		const O           = 'OR';
		const NO          = 'NOT';
				
		protected $tipos = array (
		'decimal'     =>  ['tipo' => 'decimal',  'dimension' => '11,5'],
		'moneda'      =>  ['tipo' => 'decimal',  'dimension' => '11,2'],
		'booleano'    =>  ['tipo' => 'int',      'dimension' => '1'],
		'entero'      =>  ['tipo' => 'int',      'dimension' => '12'],
		'caracteres'  =>  ['tipo' => 'varchar',  'dimension' => '250'],
		'caracter'    =>  ['tipo' => 'varchar',  'dimension' => '250'],
		'texto'       =>  ['tipo' => 'text'],
		'fecha'       =>  ['tipo' => 'datetime'],
		'id'	      =>  ['tipo' => 'int',  'dimension' => '11', 'extra' => 'not null auto_increment primary key']	
		);
		
		
		
		
		
				
		
		




		public $seleccionar =
		'SELECT {seleccion: {nexo_coma} {tabla}.{columna}}' . PHP_EOL .
		' FROM {nombre} ' .  PHP_EOL .
		 '{tiene_relacion? INNER JOIN} {relacion: {nexo_and} {tabla_relacionada} ON {tabla}.{columna} = {tabla_relacionada}.{columna_relacionada}  } ' . PHP_EOL .
		 '{tiene_condicion? WHERE} {condicion: {nexo_and} {tabla}.{columna} {comparacion} "{valor}" } '  . PHP_EOL .
		 '{tiene_grupo? GROUP BY} {grupo: {nexo_and} {tabla}.{columna} }' . PHP_EOL .
		 '{tiene_orden? ORDER BY} {orden: {nexo_and} {tabla}.{columna} {orden} }' . PHP_EOL .
		 '{limite: LIMIT {inicio} {limite} }' . 
		 '; ';


		 
		public $insertar = 
			'INSERT INTO 
			{nombre} ({valor: {nexo_coma} {columna}}) ' .
			'VALUES ({valor: {nexo_coma} "{valor}"}) ' .
			'; ';


		public $actualizar = 
			'UPDATE {nombre} ' .			
			'SET {valor: {nexo_coma} {columna} = "{valor}"} ' .
			'{tiene_condicion? WHERE} {condicion: {nexo_and} {columna} {comparacion} "{valor}" } '  . 
			'{limite: LIMIT {inicio} {limite} }' . 
			'; ';
		

		public $eliminar = 
			'DELETE {nombre} ' .			
			'{tiene_condicion? WHERE} {condicion: {nexo_and} {columna} {comparacion} "{valor}" } '  . 
			'{limite: LIMIT {inicio} {limite} }' . 
			'; ';





		public $crear_tabla = 
			'CREATE TABLE IF NOT EXISTS {nombre} ' .			
			//'( {seleccion: {nexo_coma} {columna} {tipo} {dimension? ({dimension})} {extra} })' . PHP_EOL .
			'( {seleccion: {nexo_coma} {columna} {tipo} {dimension_parentesis} {extra} })' . PHP_EOL .
			'; ';

		public $eliminar_tabla = 
			'DROP TABLE IF NOT EXISTS {nombre} ' .						
			'; ';

		public $obtener_columnas = 
			'SHOW COLUMNS FROM {nombre} ' .
			'; ';
			
		public $agregar_columnas = 
			'ALTER TABLE  {nombre} ' .			
			'{seleccion:  {nexo_coma} ADD COLUMN IF NOT EXISTS {columna} } ' . PHP_EOL .
			'; ';

		public $crear_base = 
			'CREATE DATABASE IF NOT EXISTS  {base}  DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci' .			
			'; ';

		
		
		public $sufijo = '' ; // seguridad









		
		public function seleccionar ($tabla) {
			if ($tabla->columnas () == array())
				$tabla->seleccion ('*');			
			$consulta = $this->crear_consulta ($tabla, $this->seleccionar, $this->sufijo);	
			$this->ejecutar ($consulta);
			return $tabla;
		}

		public function insertar ($tabla) {
			$consulta = $this->crear_consulta ($tabla, $this->insertar, $this->sufijo);	
			$this->ejecutar ($consulta);
			return $tabla;
        }
		
		public function actualizar ($tabla) {
			$consulta = $this->crear_consulta ($tabla, $this->actualizar, $this->sufijo);	
			$this->ejecutar ($consulta);
			return $tabla;
		}
		
		public function eliminar ($tabla) {
			$consulta = $this->crear_consulta ($tabla, $this->eliminar, $this->sufijo);	
			$this->ejecutar ($consulta);
			return $tabla;
        }





		// Todas las demás funciones son iguales, entonces.....
		// Ejecuta el resto de las consultas con funciones no declaradas.
		public function __call ($funcion, $argumentos) {
                
			$tabla = null;
			$plantilla = $this->$funcion;
			$sufijo = $this->sufijo;

			if (isset ($argumentos[0]))
				$tabla = $argumentos[0];
			else
				return;
			
			
			$consulta = $this->crear_consulta ($tabla, $plantilla, $sufijo);
			$this->ejecutar ($consulta);
			return $tabla;
						
		}

		
		
		
        
		
		
		protected function comparacion ($comparacion) {
			switch ($comparacion) {
				case JBDDDato :: IGUAL       : return SELF :: IGUAL;
				case JBDDDato :: DISTINTO    : return SELF :: DISTINTO;
				case JBDDDato :: MAYOR       : return SELF :: MAYOR;
				case JBDDDato :: MENOR       : return SELF :: MENOR;
				case JBDDDato :: MAYOR_IGUAL : return SELF :: MAYOR_IGUAL;
				case JBDDDato :: MENOR_IGUAL : return SELF :: MENOR_IGUAL;
				case JBDDDato :: CONTIENE    : return SELF :: CONTIENE;
				case JBDDDato :: ENTRE       : return SELF :: ENTRE;
			}
		}




		// conexión //

		
		
		public function conectar ($base, $usuario = 'root', $clave = '', $direccion = 'localhost') {
			$this->base = $base;
			$this->conexion = mysqli_connect($direccion, $usuario, $clave, $base) 
				or $this->generar_error(mysqli_error($this->conexion));
		}



		// ejecuta la consulta en MySQL y devuelve tuplas ----------
		public function ejecutar ($consulta) {
			if ($this->conexion == null)
				return null;
			
			parent::ejecutar ($consulta);
			
			$this->liberar();
			
			$this->tuplas = mysqli_query($this->conexion, $consulta) 
			or $this->generar_error(mysqli_error($this->conexion));

			$this->total = mysqli_affected_rows ($this->conexion);
			$this->historial  ('Filas: ' . $this->total, 'mensaje');
			return $this->tuplas;
		}

		
		




		


		public function liberar () {
			if ($this->existeTupla()== false)
				return;
			mysqli_free_result($this->tuplas);
		}

		public function utf8 () {
			mysqli_set_charset ($this->conexion, "utf8");	
		}
		
		public function desconectar () {
			mysqli_close($this->conexion) or $this->generar_error(mysqli_error($this->conexion));
		}
		
		protected function generar_error ($mensaje_error = null) {
			if ($mensaje_error == null)
				mysqli_error($this->conexion);
			parent::generar_error ($mensaje_error);

		}


		public function siguienteTupla () {
			if ($this->existeTupla()== false)
				return null;
			$this->tupla = mysqli_fetch_assoc($this->tuplas);
			return $this->tupla;
		}
		
		
		

		
		
		public function bloquear    () { return mysqli_begin_transaction ($this->conexion) or $this->generar_error(); }
		public function desbloquear () { return mysqli_commit ($this->conexion) or $this->generar_error(); }










	}



?>
