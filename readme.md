# JBDD

El objetivo es crear una capa que permita a un programa en PHP funcionar
de manera independiente de la base de datos usada.
Por ejemplo:

	$bdusuario->seleccionar('nombre', 'clave');
	
Detrás de la consulta podría estar Mongo DB, o incluso un archivo de texto, 
en éste caso hay una base de datos MariaDB.


	
El programa se compone de unos pocos archivos que contienen las clases que conforman este programa.


### JBDD 

Para uso del programador.
Es la clase "principal".
Agrupa todas las bases de datos soportadas.
Desde aquí se puede conectar a cualquier tipo de base de datos.

	// Crea el objeto manejador de base de datos
	$bd = new jbdd ('mysql');	
	
	// Establece la conexión con la base de datos
	$bd = conectar ();
	
	// Habilita el modo de prueba para ver en pantalla las consultas generadas
	$bd = ver ();
	
	// Obtiene un objeto (JBDDTabla) que representa la tabla "usuarios"
	$bdusuario = $bd->tabla('usuarios');
	
	
	
### JBDDGenerico	

Para uso interno del programa.
Representa una base de datos genérica.
Contiene métodos comunes que debe tener cualquier base de datos.
También contiene herramientas para la creación de plantillas de consultas.
De aquí se extienden bases de datos específicas.


    // Verifica si existen tuplas
    $bdusuario->existeTupla ();

    // Obtiene las tuplas (resultado de una selección)
    $bdusuario->tuplas ();
    
    
    
    
### JBDDTabla

Para uso del programador.
Es un manejador de tablas (con información de consultas).
Desde aquí se insertará, actualizará, eliminará y seleccionará datos.
Desde aquí se realizará todas las operaciones con tablas.
También se usará para emitir (imprimir) respuestas.

    // Obtiene un objeto manejador de tablas.
    $bdusuario = $bd->tabla('usuarios');
    
    
    
    
    
### JBDDMySql

Para uso interno del programa. 
Clase manejadora de base de datos MySQL.
Convierte datos genéricos (JBDDTabla's) en consultas MySQL.
Aquí se definen las consultas mediante plantillas.


    total (afectados)
    liberar
    desconectar
    siguienteTupla
    bloquear
    desbloquear


    
### JBDDMySQLExtra

Para uso interno del programa.
Agrega funciones adicionales a la clase JBDDMySql.


    insertar_si_no_existe
    actualizar_o_insertar
    existe
    obtener_arreglo
    obtener_lista
    imprimir

		

		
		




# Proyectos en los que se utiliza este programa

Seguime 
https://gitlab.com/javipc/seguime

Foto Reloj
https://t.me/relojbot

Listagrama
https://listagrama.000webhostapp.com
https://t.me/listagrama



No es propósito de este programa ejecutar consultas complejas, 
surgió y se creó para uso de mis proyectos y para facilitar la tarea del manejo de base la base de datos.


# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



